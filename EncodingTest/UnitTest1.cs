﻿using System;
using Encoding;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EncodingTest
{
    [TestClass]
    public class UnitTest1
    {
        private readonly EncoderBase encoder = new MyEncoder();

        [TestMethod]
        public void TestEncode()
        {
            encoder.Key = 3;
            var str = "ВРАГ БУДЕТ РАЗБИТ";
            Assert.AreEqual("ВГУТАИР Д ЗТАБЕРБ", encoder.Encode(str));
        }

        [TestMethod]
        public void TestDecode()
        {
            encoder.Key = 4;
            var str1 = "ВРАГ БУДЕТ РАЗБИТ";
            var encoded1 = encoder.Encode(str1);
            Assert.AreEqual(str1, encoder.Decode(encoded1));

            encoder.Key = 5;
            var str2 = "Hello my english is bad London is the capital of great Britain";
            var encoded2 = encoder.Encode(str2);
            Assert.AreEqual(str2, encoder.Decode(encoded2));

            encoder.Key = 3;
            var str3 = "ПРИВЕТ АНДРЕЙ";
            var encoded3 = encoder.Encode(str3);
            Assert.AreEqual(str3, encoder.Decode(encoded3));

            var str4 = "ПРИВЕТ АНДРЕ";
            var encoded4 = encoder.Encode(str4);
            Assert.AreEqual(str4, encoder.Decode(encoded4));

            var str5 = "ПРИВЕТ АНДР";
            var encoded5 = encoder.Encode(str5);
            Assert.AreEqual(str5, encoder.Decode(encoded5));
        }

        [TestMethod]
        public void TestWrongDecode()
        {
            encoder.Key = 5;
            var str = "Hello my english is bad London is the capital of great Britain";
            var encoded = encoder.Encode(str);
            encoder.Key = 4;
            Assert.AreNotEqual(str, encoder.Decode(encoded));
        }
    }
}
