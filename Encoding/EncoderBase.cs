﻿namespace Encoding
{
    public abstract class EncoderBase
    {
        public int Key { get; set; }
        public abstract char[,] EncodeData { get; }
        public abstract string Encode(string str);
        public abstract string Decode(string str);
    }
}