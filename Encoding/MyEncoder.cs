﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encoding
{
    internal enum Process
    {
        Encode,
        Decode
    }

    public class MyEncoder : EncoderBase
    {
        private int rows = 3;
        private int columns;
        private char[,] array;

        public override char[,] EncodeData => array;

        public MyEncoder()
        {
            Key = 3;
        }

        public override string Encode(string str)
        {
            return Process(str, Encoding.Process.Encode);
        }

        public override string Decode(string str)
        {
            return Process(str, Encoding.Process.Decode);
        }

        private void FillArray(string str, Process process)
        {
            rows = Key;
            columns = str.Length / rows + 1;

            array = new char[rows, columns];
            if (process == Encoding.Process.Encode)
            {
                rows = columns;
                columns = Key;
            }

            var position = 0;

            if (process == Encoding.Process.Decode)
            {
                var nulls = rows * columns - str.Length;
                for (int i = 0; i < nulls; i++)
                    array[rows - 1 - i, columns - 1] = '$';
            }

            for (var i = 0; i < rows; i++)
                for (var j = 0; j < columns; j++)
                {
                    if (process == Encoding.Process.Decode)
                        if (array[i, j] == '$') continue;
                    if (position < str.Length)
                    {
                        FillCell(i, j, str[position], process);
                    }
                    else
                    {
                        FillCell(i, j, '$', process);
                    }
                    position++;
                }

        }

        private void FillCell(int i, int j, char value, Process process)
        {
            if (process == Encoding.Process.Encode)
                array[j, i] = value;
            else array[i, j] = value;
        }

        private string Process(string str, Process process)
        {
            FillArray(str, process);
            var result = "";

            for (var i = 0; i < columns; i++)
            for (var j = 0; j < rows; j++)
            {
                if (process == Encoding.Process.Encode)
                {
                    if (array[i, j] != '$') result += array[i, j];
                }
                else
                {
                    if (array[j, i] != '$') result += array[j, i];
                }
            }
            return result;
        }
    }
}
