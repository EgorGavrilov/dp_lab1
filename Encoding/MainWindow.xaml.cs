﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Encoding
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly EncoderBase encoder = new MyEncoder();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void PopulateDataGrid()
        {
            DataTable dataTable = new DataTable();
            for (int j = 0; j < encoder.EncodeData.GetLength(1); j++)
                dataTable.Columns.Add(new DataColumn("Column " + j.ToString()));

            for (int i = 0; i < encoder.EncodeData.GetLength(0); i++)
            {
                var newRow = dataTable.NewRow();
                for (int j = 0; j < encoder.EncodeData.GetLength(1); j++)
                    newRow["Column " + j.ToString()] = encoder.EncodeData[i, j];
                dataTable.Rows.Add(newRow);
            }
            dataGrid.ItemsSource = dataTable.DefaultView;
        }

        private void encodeButton_Click(object sender, RoutedEventArgs e)
        {
            var key = 3;
            int.TryParse(encodeKeyTextBox.Text, out key);
            encoder.Key = key;
            var result = encoder.Encode(encodeTextBoxData.Text);
            encodeTextBoxResult.Text = result;

            PopulateDataGrid();
        }

        private void decodeButton_Click(object sender, RoutedEventArgs e)
        {
            var key = 3;
            int.TryParse(decodeKeyTextBox.Text, out key);
            encoder.Key = key;
            var result = encoder.Decode(decodeTextBoxData.Text);
            decodeTextBoxResult.Text = result;

            PopulateDataGrid();
        }

        private void copyToEncodeButton_Click(object sender, RoutedEventArgs e)
        {
            encodeTextBoxData.Text = decodeTextBoxResult.Text;
        }

        private void copyToDecodeButton_Click(object sender, RoutedEventArgs e)
        {
            decodeTextBoxData.Text = encodeTextBoxResult.Text;
        }
    }
}
